let aboutButt = document.querySelector('button[name=about]');
let navbarBrand = document.querySelector('a[name=navbarBrand]');
let work = document.querySelector('button[name=work]');
let workSec = document.querySelector('#workSec');
let welcome = document.querySelector('#welcome');
let aboutSec = document.querySelector('#aboutSec');
let presentation = document.querySelector('#presentation')

let navBar = document.querySelector('#navbarP')
document.addEventListener('scroll', () => {
    if (window.scrollY > 30) {
        navBar.classList.add('navbar-active')
    } else {
        navBar.classList.remove('navbar-active')
    }
})

// if(navbarBrand){
aboutButt.onclick = function () {

    welcome.classList.add('d-none', 'position-absolute');
    aboutSec.classList.remove('d-none', 'position-absolute');
    workSec.classList.add('d-none', 'position-absolute');
    presentation.classList.add('d-none', 'position-absolute');
    portfolioWb.classList.add('d-none', 'position-absolute');
    videoartPage.classList.add('d-none');
}
work.onclick = function () {

    welcome.classList.add('d-none', 'position-absolute');
    workSec.classList.remove('d-none', 'position-absolute');
    aboutSec.classList.add('d-none', 'position-absolute');
    presentation.classList.add('d-none', 'position-absolute');
    portfolioWb.classList.add('d-none', 'position-absolute');
    videoartPage.classList.add('d-none');
}


navbarBrand.onclick = function () {

    welcome.classList.remove('d-none', 'position-absolute');
    aboutSec.classList.add('d-none', 'position-absolute');
    presentation.classList.remove('d-none');
    portfolioWb.classList.add('d-none', 'position-absolute');
    workSec.classList.add('d-none', 'position-absolute');
    videoartPage.classList.add('d-none');
}



let masthead = document.querySelector('#masthead');
// console.log(masthead)

document.addEventListener('mousemove', (event) => {
    const width = window.innerWidth
    // console.log(width)
    // console.log(`Mouse X: ${event.clientX}, Mouse Y: ${event.clientY}`);
    if (masthead) {
        if (width >= 1400) {
            if (event.clientX >= 700) {
                masthead.classList.remove('masthead');
                masthead.classList.add('masthead2');
            } else {
                masthead.classList.remove('masthead2');
                masthead.classList.add('masthead');
            }
        } else if (width >= 1200 && width < 1400) {
            if (event.clientX >= 600) {
                masthead.classList.remove('masthead');
                masthead.classList.add('masthead2');
            } else {
                masthead.classList.remove('masthead2');
                masthead.classList.add('masthead');
            }
        } else if (width >= 800 && width < 1200) {
            if (event.clientX >= 400) {
                masthead.classList.remove('masthead');
                masthead.classList.add('masthead2');
            } else {
                masthead.classList.remove('masthead2');
                masthead.classList.add('masthead');
            }
        } else if (width >= 600 && width < 800) {
            if (event.clientX >= 300) {
                masthead.classList.remove('masthead');
                masthead.classList.add('masthead2');
            } else {
                masthead.classList.remove('masthead2');
                masthead.classList.add('masthead');
            }
        }
    }
});
// }
document.addEventListener('touchstart', (event) => {
    // console.log(event.touches[0].clientX)
    const width = window.innerWidth
    if (masthead) {
        if (width >= 550) {
            if (event.touches[0].clientX >= 290) {
                masthead.classList.remove('masthead');
                masthead.classList.add('masthead2');
            } else {
                masthead.classList.remove('masthead2');
                masthead.classList.add('masthead');
            }
        } else if (width >= 360 && width < 550) {
            if (event.touches[0].clientX >= 200) {
                masthead.classList.remove('masthead');
                masthead.classList.add('masthead2');
            } else {
                masthead.classList.remove('masthead2');
                masthead.classList.add('masthead');
            }
        } else if (width < 360) {
            if (event.touches[0].clientX >= 140) {
                masthead.classList.remove('masthead');
                masthead.classList.add('masthead2');
            } else {
                masthead.classList.remove('masthead2');
                masthead.classList.add('masthead');
            }
        }
    }
})

// project

let webDev = document.querySelector('a[name=webDev]');
let portfolioWb = document.querySelector('#portfolioWb');
webDev.onclick = () => {
    workSec.classList.add('d-none', 'position-absolute');
    portfolioWb.classList.remove('d-none');
}
let portfolioWip = document.querySelector('#portfolioWip');
document.addEventListener('scroll', () => {
    if (window.scrollY > 310) {
        portfolioWip.classList.remove('d-none')
    } else {
        portfolioWip.classList.add('d-none')
    }
})

// video art

let videoArt = document.querySelector('a[name=videoArt]');
let videoartPage = document.querySelector('#videoartPage');
videoArt.onclick = () => {
    workSec.classList.add('d-none', 'position-absolute');
    videoartPage.classList.remove('d-none');
}


// let videos = [
//     { url: './video/cyberhowling.mp4'  },
//     { url: './video/juda.mp4' },
//     { url: './video/gaia.mp4'  },
//     { url: './video/rifinito3.mp4'  },
//     { url: './video/flosdeserti2.mp4'  },
//     { url: './video/marinella.mp4'  },
//     { url: './video/POLLE.mp4'  },
  
// ]

// videos.forEach(video => {
// let videoWrapper = document.querySelector('.carousel');
// let card = document.createElement('div');
// card.classList.add('px-0', 'px-sm-5')
//     card.innerHTML = `
                    
//                         <video class="img-fluid rounded px-0 px-sm-5 mx-0 mx-sm-5" autoplay muted loop>
//                         <source src=${video.url} type="video/mp4">
//                         <!-- <source src="movie.ogg" type="video/ogg"> -->
//                         Your browser does not support the video tag.

//                         </video>
//                     `

//      videoWrapper.appendChild(card)
// });

// let x = document.cookie
// console.log(x)

{/* <div class="px-0 px-sm-5">
<video class="img-fluid rounded px-0 px-sm-5 mx-0 mx-sm-5" autoplay muted loop>
    <source src="./video/andrea.mp4" type="video/mp4">
    <!-- <source src="movie.ogg" type="video/ogg"> -->
    Your browser does not support the video tag.

</video>
</div>

<div class="px-0 px-sm-5">
<video class="img-fluid rounded px-0 px-sm-5" autoplay muted loop>
    <source src="./video/andrea.mp4" type="video/mp4">
    <!-- <source src="movie.ogg" type="video/ogg"> -->
    Your browser does not support the video tag.

</video>
</div>

<div class="px-0 px-sm-5">
<video class="img-fluid rounded px-0 px-sm-5" autoplay muted loop>
    <source src="./video/andrea.mp4" type="video/mp4">
    <!-- <source src="movie.ogg" type="video/ogg"> -->
    Your browser does not support the video tag.

</video>
</div>

<div class="px-0 px-sm-5">
<video class="img-fluid rounded px-0 px-sm-5" autoplay muted loop>
    <source src="./video/andrea.mp4" type="video/mp4">
    <!-- <source src="movie.ogg" type="video/ogg"> -->
    Your browser does not support the video tag.

</video>
</div> */}


{/* <li class="col-12 col-sm-3 m-3 item2">
                        <div class="text-center ">
                            <h2 class="tc-main py-2 fs-5"> Cyber howling </h2>
                        </div>
                        <video class="img-fluid rounded pt-3" autoplay muted loop>
                            <source src="./video/cyber howling.mp4" type="video/mp4">
                            <!-- <source src="movie.ogg" type="video/ogg"> -->
                            Your browser does not support the video tag.

                        </video>
                </li>
                <li class="col-12 col-sm-3 m-3 item2">
                        <div class="text-center ">
                            <h2 class="tc-main py-2 fs-5"> Onda </h2>
                        </div>
                        <video class="img-fluid rounded pt-3" autoplay muted loop>
                            <source src="./video/juda.mp4" type="video/mp4">
                            <!-- <source src="movie.ogg" type="video/ogg"> -->
                            Your browser does not support the video tag.

                        </video>
                </li>
                <li class="col-12 col-sm-3 m-3 item2">
                        <div class="text-center ">
                            <h2 class="tc-main py-2 fs-5"> Travel </h2>
                        </div>
                        <video class="img-fluid rounded pt-3" autoplay muted loop>
                            <source src="./video/gaia.mp4" type="video/mp4">
                            <!-- <source src="movie.ogg" type="video/ogg"> -->
                            Your browser does not support the video tag.

                        </video>
                </li>
                <li class="col-12 col-sm-3 m-3 item2">
                        <div class="text-center ">
                            <h2 class="tc-main py-2 fs-5"> Sweet </h2>
                        </div>
                        <video class="img-fluid rounded pt-3" autoplay muted loop>
                            <source src="./video/rifinito 3.mp4" type="video/mp4">
                            <!-- <source src="movie.ogg" type="video/ogg"> -->
                            Your browser does not support the video tag.
                        </video>
                </li>
                <li class="col-12 col-sm-3 m-3 item2">
                        <div class="text-center ">
                            <h2 class="tc-main py-2 fs-5"> Flos deserti </h2>
                        </div>
                        <video class="img-fluid rounded pt-3" autoplay muted loop>
                            <source src="./video/flos deserti 2.mp4" type="video/mp4">
                            <!-- <source src="movie.ogg" type="video/ogg"> -->
                            Your browser does not support the video tag.

                        </video>
                </li>
                <li class="col-12 col-sm-3 m-3 item2">
                        <div class="text-center ">
                            <h2 class="tc-main py-2 fs-5"> Equilibrium </h2>
                        </div>
                        <video class="img-fluid rounded pt-3" autoplay muted loop>
                            <source src="./video/POLLE.mp4" type="video/mp4">
                            <!-- <source src="movie.ogg" type="video/ogg"> -->
                            Your browser does not support the video tag.

                        </video>
                </li>
                <li class="col-12 col-sm-3 m-3 p-4 item2">
                        <div class="text-center ">
                            <h2 class="tc-main py-2 fs-5"> Psyc </h2>
                        </div>
                        <video class="img-fluid rounded pt-3" autoplay muted loop>
                            <source src="./video/marinella.mp4" type="video/mp4">
                            <!-- <source src="movie.ogg" type="video/ogg"> -->
                            Your browser does not support the video tag.

                        </video>
                </li> */}